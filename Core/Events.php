<?php
namespace Bender\dre_AdminRights\Core;

// use Exception;
// use Doctrine\DBAL\Driver\PDOIbm\Driver\PDOException;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Exception\DatabaseException;
use OxidEsales\Eshop\Core\FileCache;
use OxidEsales\Eshop\Core\ConfigFile;
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\Exception\ExceptionHandler;
use OxidEsales\Eshop\Core\DbMetaDataHandler;

class Events {

    public static function onActivate(){

        $blRight = false;
        $count = 0;
        $oDb = DatabaseProvider::getDb();

        $meta = oxNew( DbMetaDataHandler::class );
        $exception = 0;
        $exeptionHandler = oxNew( ExceptionHandler::class );
        $dependencies = array();
        $count = 0 ;

        /**
         * Check ob Feld drerestrict in oxuser vorhanden ist.
         *
         */
        if($meta->fieldExists('drerestrict','oxuser')){
            $dependencies['oxuser']['drerestrict'] = 'vorhanden';
            $count++;
        }else{
            try{
                $oDb->execute('ALTER TABLE oxuser ADD DRERESTRICT TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL');
            }catch ( DatabaseException $exception ) {
                $dependencies['oxuser']['drerestrict'] = 'fehlt';
                $blRight = true;
                $exeptionHandler->setRenderer($exception);
            }
        }

        /**
         * Check ob Feld dreuserid in oxarticles vorhanden ist.
         */
        if($meta->fieldExists('dreuserid','oxarticles')){
            $dependencies['oxarticles']['dreuserid'] = 'vorhanden';
            $count++;
        }else{
            try {
                $oDb->execute('ALTER TABLE oxarticles ADD DREUSERID CHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT \'\' NOT NULL');
            } catch (DatabaseException  $exception) {
                $dependencies['oxarticles']['dreuserid'] = 'fehlt';
                $blRight = true;
                $exeptionHandler->setRenderer($exception);
            }
        }
        /**
         * Wenn beide Abhängigkeiten erfüllt sind die Funktion verlassen.
         */
        if ($count === 2){
            return;
        }


        $oConf = Registry::getConfig();
        $oLang = Registry::getLang();
        $otplLang = $oLang->getTplLanguage();

        /**
         * Wenn bei der Datenbank änderung ein Fehler aufgetreten ist, disen anzeigen, Log erstellen und Funktion
         * verlassen.
         */
        if ($blRight) {
            if (!is_int($exception) && (int) $exception->getCode()){
                $string = '<hr>(' . $exception->getCode() . ') ' . $exception->getMessage();
            } else {
                $string = '';
            }
            $ob37d4424b = $oLang->translateString('USER_RIGHTS_DB_SETUP_ERROR', $otplLang) . '<br>' . $obe800f1e9 . $string ;


            $oUtils = Registry::getUtils();
            $oUtils->showMessageAndExit($ob37d4424b);
        }

        /**
         * Die gecachten Files überprüfen, ob Einträge vorhanden sind, die dem neuen Datenbankzustand wiedersprechen,
         * diese entsprechend löschen.
         */
        $ob1d88f7a3 = $oConf->getConfigParam('sCompileDir');
        if ($ob2581733c = glob($ob1d88f7a3 . 'oxpec_fieldnames_oxuser_allviews*.txt')) {
            foreach ($ob2581733c as $obd8155113) {
                @unlink($obd8155113);
            }
        }

        if ($ob2581733c = glob($ob1d88f7a3 . 'oxpec_oxuser_allfields_*.txt')) {
            foreach ($ob2581733c as $obd8155113) {
                @unlink($obd8155113);
            }
        }

        if ($ob2581733c = glob($ob1d88f7a3 . 'oxpec_fieldnames_oxarticles_details*.txt')){
            foreach ($ob2581733c as $obd8155113) {
                @unlink($obd8155113);
            }
        }

        if ($ob2581733c = glob($ob1d88f7a3 . 'oxpec_oxarticles_allfields_*.txt')) {
            foreach ($ob2581733c as $obd8155113) {
                @unlink($obd8155113);
            }
        }

        /**
         * Mit dem aktuell angemeldetem User versuchen die Views zu updaten, wenn das schief geht Message anzeigen
         * und Funktion verlassen.
         */
        $ob7aac9a05 = oxNew('oxDbMetaDataHandler');
        if (!$ob7aac9a05->updateViews(array('oxarticles'))) {
            $ob37d4424b = $oLang->translateString('USER_RIGHTS_DB_SETUP_ERROR', $otplLang) . '<br>(updateViews)';
            $oUtils = Registry::getUtils();
            $oUtils->showMessageAndExit($ob37d4424b);
        }

        /**
         * Die Views mit einer alternativen Klasse updaten, den Cache leeren und die Erfolgsmeldung ausgeben.
         */
        $oView = oxNew('Tools_List');
        $oView->updateViews();

        self::clearCache();

        $ob3e8f6fd4 = $oLang->translateString('USER_RIGHTS_DB_SETUP_DONE', $otplLang);

        echo '<script type="text/javascript">alert("' . $ob3e8f6fd4 . '");</script>';



    }

    public static function onDeactivate(){
        self::clearCache();
    }

    /**
     * Löscht den TMP-Ordner sowie den Smarty-Ordner
     */
    protected static function clearCache()
    {
        /** @var FileCache $fileCache */
        $fileCache = oxNew( FileCache::class );
        $fileCache::clearCache();

        /** Smarty leeren */
        $tempDirectory = Registry::get( ConfigFile::class )->getVar("sCompileDir");
        $mask = $tempDirectory . "/smarty/*.php";
        $files = glob($mask);
        if (is_array($files)) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        }
    }
}
