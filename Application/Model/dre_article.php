<?php
namespace Bender\dre_AdminRights\Application\Model;

class dre_article extends dre_article_parent {

	protected $adminRights = null;
	
	public function getArtOwner($sOxuserName = null) {

	    if ($this->adminRights == null) {
			$this->adminRights = false;
			if ($sUserId = $this->oxarticles__dreuserid->value) {
                $user = oxNew(\OxidEsales\Eshop\Application\Component\UserComponent::class);
                $oUser = $user->getUser();
				//$oUser = oxNew('oxuser');
				if ($oUser->load($sUserId)) {
					$this->adminRights = $oUser;
				}
			}
		}

		if ($this->adminRights && $sOxuserName) {
			return $this->adminRights->getFieldData($sOxuserName);
		}

		return $this->adminRights;
	}
}
