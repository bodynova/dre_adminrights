<?php

namespace Bender\dre_AdminRights\Application\Controller\Admin;

use OxidEsales\Eshop\Core\ShopVersion;

class dre_user_main extends dre_user_main_parent {

	public function render() {

		$ob9700c473 = $this->ob383446c4();
		$ob5120aebc = parent::render();
		$obb01e62e9 = $this->getUser();
		$ob5abfa39d = array();
		
		if ($obf78c10d4 = $obb01e62e9->oxuser__drerestrict->value) {
			$ob5abfa39d = explode(',', $obf78c10d4);
		}
		
		if ($ob9700c473 === $obb01e62e9->getId()) {
			$obcba355fc = $this->_aViewData['rights'];
			unset($obcba355fc[0]);
			$this->_aViewData['rights'] = $obcba355fc;
			
		} elseif (in_array('noeditadmin', $ob5abfa39d , true)) {
			$obcba355fc = $this->_aViewData['rights'];
			unset($obcba355fc[1]);
			$this->_aViewData['rights'] = $obcba355fc;
		}
		return $ob5120aebc;
	}
	
	protected function ob383446c4() {

        $shopversion = oxNew(ShopVersion::class)->getVersion();

		if (version_compare( $shopversion, '4.5.0', '>=')) {
			return $this->getEditObjectId();
		}
		
		if (null === ($ob3a942e4a = $this->_sEditObjectId)) {
			//oxRegistry::getConfig()->getRequestParameter('oxid'); // new
			oxRegistry::getConfig()->getConfigParam("oxid"); // old
		} elseif (null === ($ob3a942e4a = oxRegistry::getConfig()->getRequestParameter('oxid'))) {
			//$ob3a942e4a = oxSession::getVar("saved_oxid"); //old
			$ob3a942e4a = oxRegistry::getSession()->getVariable('saved_oxid'); // new
		}
		return $ob3a942e4a;
	}
}
