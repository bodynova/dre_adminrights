<?php

namespace Bender\dre_AdminRights\Application\Controller\Admin;

use Bender\dre_AdminRights\Core\Events;
use OxidEsales\Eshop\Core\ShopVersion;

use OxidEsales\Eshop\Core\Request;
use OxidEsales\Eshop\Core\Registry;


class dre_user_rights extends \OxidEsales\Eshop\Application\Controller\Admin\AdminController {
	public function render() {
	    
		parent::render();
		
		$sUser = $this->checkRequirements();
		
		if ($sUser !== '-1' && isset($sUser)) {
		    
			if ($sUser !== 'oxdefaultadmin' && $sUser == $this->getUser()->getId()){
				$this->_aViewData['noAccess'] = true;
			}
			
			$oUser = oxNew('oxuser');
			$oUser->load($sUser);
			
			if (($sEditRights = $oUser->oxuser__drerestrict->value) === null) {
                Events::onActivate();
			}
			
			$arrRights = array();
			
			if ($sEditRights = $oUser->oxuser__drerestrict->value) {
				$arrRightsChange = explode(',', $sEditRights);
				foreach ($arrRightsChange as $sRight) $arrRights[$sRight] = true;
			}
			$this->_aViewData['menustructure'] = $this->getNavigation()->getDomXml()->documentElement->childNodes;
            /*
			echo '<pre>';
			print_r($arrRights);
			die();
            */


			$this->_aViewData['edit'] = $oUser;
			$this->_aViewData['restrict'] = $arrRights;
			$this->_aViewData['chknoeditadmin'] = !empty($arrRights['noeditadmin']);
			$this->_aViewData['chknoforeignart'] = !empty($arrRights['noforeignart']);
			$this->_aViewData['chknoservicearea'] = !empty($arrRights['noservicearea']);
			$this->_aViewData['chknodeltmp'] = !empty($arrRights['nodeltmp']);
		}
		if (!$this->_allowAdminEdit($sUser)) {
			$this->_aViewData['readonly'] = true;
		}
			
		return "dre_user_rights.tpl";
	}
	
	public function save()
	{
	    $oConf = Registry::getConfig();
		parent::save();
		$sUser = $this->checkRequirements();
		if (!$this->_allowAdminEdit($sUser)) {
			return false;
		}

		$sEditValue = Registry::get(Request::class)->getRequestEscapedParameter( 'editval' );
		$sEditRights = trim($sEditValue['oxuser__drerestrict'], " ,\t\n\r");

		$arrRights = array();

		$oUser = oxNew('oxuser');

		if ($sUser !== '-1') {
			$oUser->load($sUser);
		} else {
			$sEditValue['oxuser__oxid'] = null;
		}

		$sEditValue['oxuser__oxactive'] = $oUser->oxuser__oxactive->value;

		if (!$sEditRights) {
			if (!is_array($arrRestricts = Registry::get(Request::class)->getRequestEscapedParameter( 'restrict' ))) {  // oxConfig::getParameter('restrict'))) $arrRestricts = array();
				$arrRestricts = array();
			}
			
		} else {
			$arrRestricts = explode(',', $sEditRights);
		}
		
		foreach ($arrRestricts as $sRestrict) {

			$sRestrict = trim($sRestrict);

			if (preg_match("/^[\w ]+$/", $sRestrict)) {
				$arrRights[] = $sRestrict;
			}
		}

		$sEditValue['oxuser__drerestrict'] = implode(',', array_unique($arrRights));
		$oUser->assign($sEditValue);

		if ($oUser->save()) {
			$this->_aViewData['savedone'] = true;
		}
	}
	
	protected function checkRequirements()
	{
	    $shopversion = oxNew( ShopVersion::class )->getVersion();
	    //$oConf = Registry::getConfig();

		if(version_compare( $shopversion , '4.5.0', '>=')){
			return $this->getEditObjectId();
		}

		if (null === ( $sEditOxid = $this->_sEditObjectId )) {
			if ( null === ( $sEditOxid = Registry::get( Request::class )->getRequestEscapedParameter('oxid'))) {
				$sEditOxid = Registry::getSession()->getVariable('saved_oxid');
			}
		}

		return $sEditOxid;
	}
}
