<?php
$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTITFIER = STRING
// -------------------------------
$aLang = array(
    'charset'                                  => 'UTF-8',
    'SHOP_MODULE_GROUP_dre_main'               => 'Allgemein',
    'SHOP_MODULE_blDreArShowArticleOwner'      => 'Anzeige der Artikel-Besitzer im Admin (Standard: nein)',

    'tbcluser_rights'                          => 'Rechte',
    'USER_RIGHTS_NO_DEFAULT_ADMIN'             => 'Die Rechte des Hauptadministrators können nicht eingeschränkt werden!',
    'USER_RIGHTS_ONLY_ADMINS'                  => 'Dieses Feature kann nur mit Admin-Konten genutzt werden!',
    'USER_RIGHTS_NO_ACCESS'                    => 'Zugriff verweigert!',
    'USER_RIGHTS_INFO'                         => 'Bitte die zu sperrenden Bereiche wählen:',
    'USER_RIGHTS_WARN_NO_RIGHTS'               => 'Fehler: keine Rechte zugeordnet (das wäre ein nutzloser Account)!',
    'USER_RIGHTS_CHOOSE_ALL'                   => 'alles',
    'USER_RIGHTS_CHOOSE_NONE'                  => 'nichts',
    'USER_RIGHTS_COPY'                         => 'Kopieren:',
    'USER_RIGHTS_PASTE'                        => 'Einfügen:',
    'USER_RIGHTS_NO_EDIT_ADMIN'                => 'Darf keine anderen Admin-Konten bearbeiten oder erstellen',
    'USER_RIGHTS_NO_FOREIGN_ART'               => 'Darf nur selbst erstellte Artikel bearbeiten der löschen',
    'USER_RIGHTS_NO_SERVICE_AREA'              => 'Kein Zugriff auf die "eCommerce Services"',
    'USER_RIGHTS_NO_DEL_TMP'                   => 'Darf nicht die TMP-Files löschen (weiteres Modul)',
    'USER_RIGHTS_ARTICLE_OWNER'                => 'Besitzer',

    'USER_RIGHTS_DB_LOAD_ERROR'                => 'SQL-Setup-Datei konnte nicht geladen werden:',
    'USER_RIGHTS_DB_SETUP_ERROR'               => 'Fehler beim Ausführen des SQL-Befehls:',
    'USER_RIGHTS_DB_SETUP_DONE'                => 'Die SQL-Erweiterung wurde erfolgreich installiert!',
    'USER_RIGHTS_DB_SAVE_DONE'                 => 'Die Änderungen wurden erfolgreich gespeichert!',
    'USER_RIGHTS_DEL_ARTICLE'                  => 'Löschen'
);
