<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';


/**
 * Module information
 */
$aModule = [
    'id'           => 'dre_adminrights',
    'title'        => '<img src="../modules/bender/dre_adminrights/out/img/favicon.ico" title="Bodynova Admin Rechte Modul">odynova Admin Rechte',
    'description'  => [
        'de' => 'Modul zur Beschr&auml;nkung der Benutzer-Adminrechte',
        'en' => 'Module to restrict the admin rights of users'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'controllers'  => [
        'dre_user_rights'  =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_rights::class
    ],
    'extend'       => [
        \OxidEsales\Eshop\Application\Model\Article::class                      =>
            \Bender\dre_AdminRights\Application\Model\dre_article::class,

        \OxidEsales\Eshop\Application\Controller\Admin\NavigationTree::class    =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_navigationtree::class,

        \OxidEsales\Eshop\Application\Controller\Admin\ArticleList::class       =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_article_list::class,

        \OxidEsales\Eshop\Application\Controller\Admin\ArticleMain::class       =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_article_main::class,

        \OxidEsales\Eshop\Application\Controller\Admin\ListUser::class          =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_list_user::class,

        \OxidEsales\Eshop\Application\Controller\Admin\UserList::class          =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_list::class,

        \OxidEsales\Eshop\Application\Controller\Admin\UserMain::class          =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_main::class
    ],
    'templates' => [
        'dre_user_rights.tpl'  =>
            'bender/dre_adminrights/Application/views/admin/tpl/dre_user_rights.tpl',

        'dre_article_list.tpl'  =>
            'bender/dre_adminrights/Application/views/admin/tpl/dre_article_list.tpl'
    ],
    'settings' => [
        [
            'group' => 'dre_main',
            'name' => 'blDreArShowArticleOwner',
            'type' => 'bool',
            'value' => 'true',
            'position' => 1
        ]
    ],
    'events' => [
        'onActivate' =>
            'Bender\dre_AdminRights\Core\Events::onActivate'
    ],
    'blocks'      => [
    ]
];

?>
